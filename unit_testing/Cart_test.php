<?php
require '../classes/Product.php';
session_start();
require '../bootstrap.php';

require '../classes/Cart.php';

echo '<h1>Test Cart</h1> <br>';

$cart = new Cart();

$witbrood = new Product(1,"Wit brood","lorem klfjdslfjsd",2,0.21,5,1);
$bruinbrood = new Product(2,"bruin brood","lorem klfjdslfjsd",2,0.21,5,1);
$grijsbrood = new Product(3,"Grijs brood","lorem klfjdslfjsd",2,0.21,5,1);

$cart->addToCart($witbrood);
$cart->addToCart($bruinbrood);
$cart->addToCart($grijsbrood);

echo '<h3>Expected : 3 products each 1 time</h3> <br>';
 foreach ($_SESSION['cart'] as $product){
    echo $product['product']->naam . " : " . $product['amount'] . "<br>";
}
echo '<br>';


$cart = new Cart();
echo '<h3>Expected : No new cart created. All products still available</h3> <br>';
echo "TRUE : ";
echo !empty($_SESSION['cart']) ? "TRUE" : "FALSE";
echo '<br>';

$cart->addToCart($witbrood);
$cart->addToCart($witbrood);
$cart->addToCart($witbrood);


echo '<h3>Expected : First cart Item should have a value of 4</h3> <br>';
echo "TRUE : ";
echo $_SESSION['cart'][1]['amount'] == 4 ? "TRUE" : "FALSE";
echo '<br>';

$cart->editCart($bruinbrood, 8);

echo '<h3>Expected : product id 2 (bruinbrood) should have a value of 8 </h3><br>';
echo "TRUE : "; echo $_SESSION['cart'][1]['amount'] = 8 ? "TRUE" : "FALSE";
echo '<br>';


echo '<h3>Expected: Cart item with id 1 should be removed</h3> <br>';
$cart->removeFromCart($witbrood);
echo 'TRUE : '; echo !isset($_SESSION['cart'][1]) ? "TRUE" : "FALSE";
echo '<br>';
echo '<h3> Expected : Price Inclusive BTW</h3> <br>';
d($cart->getPriceIncBtw());

echo '<h3>Expected: Error as Cart is gone</h3>';

$cart->clear();

d($_SESSION['cart']);