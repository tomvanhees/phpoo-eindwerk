<?php

class Product{
    public $products_id;
    public $naam;
    public $omschrijving;
    public $prijs;
    public $btwtarief;
    public $amount;
    public $cartLocation;
    public $totale_score;
    public $aantal_kliks;
    public $gemiddelde;
 //WORDT GEBRUIKT OM DE CLASS TE TESTEN (HEB JE NIET NODIG)

//Constructors heb je nodig als je zegt . Nieuwe class met die en die eigenschappen. Maar als je via de databank werkt dan vult die dat automatisch in



  /*public function __construct($id,$naam,$omschrijving,$prijs,$btwtarief,$beoordeling,$aantalbeoordelingen){
        $this->id = $id;
        $this->naam = $naam;
        $this->omschrijving = $omschrijving;
        $this->prijs = $prijs;
        $this->btwtarief = $btwtarief;
        $this->beoordeling = $beoordeling;
        $this->aantalbeoordelingen = $aantalbeoordelingen;
    }*/

  private function calculateBtwTarief(){
      return ($this->btwtarief/100);
  }

  public function getPrijsIncBtwPP(){
      return number_format(round(($this->prijs + ($this->prijs * $this->calculateBtwTarief())),2,1), 2);
  }

    public function getPrijsIncBtw(){
        return round(($this->prijs + ($this->prijs * $this->calculateBtwTarief())) * $this->amount,2,1);
}
    public function setBeoordeling($beoordeling){
        $this->aantal_kliks++;
        $this->totale_score += $beoordeling;
        $this->gemiddelde = round($this->totale_score /$this->aantal_kliks,0,1);

    }
}