<?php

class Cart
{
    private $price;
    private $btw;

    public function __construct()
    {
      if (!isset($_SESSION['cart'])) {
        $_SESSION['cart'] = [];
      }

    }




    /* Product toevoegen aan de winkelwagen
    -----------------------------------------
     *
     * @parameter : $id = Id van het toe te voegen product
     * @parameter : $amount = aantal toe te voegen van het product
     * @parameter : $cartItem = bestaat uit een id van de producten die al in de winkelwagen zitten
     *
     * Controleer of de winkelwagen niet leeg is
     * =>FALSE : Voeg het product en de aantallen toe
     * =>TRUE  :
     *
     * Overloop de winkelwagen om te zien of het product er al in zit
     * =>TRUE  : Update de aantallen
     * =>FALSE : Voeg het product toe
     *
     * */

    public function addToCart($id,$amount)
    {

        if (!empty($_SESSION['cart'])) {
          $found = false;
          foreach ($_SESSION['cart'] as $key =>$cartItem){
            if($cartItem["product"] == $id){
              $_SESSION['cart'][$key]['amount'] +=$amount;
              $found = true;
              break;
            }
          }
          if(!$found){array_push($_SESSION['cart'],["product" => $id,"amount"=> $amount] );}


        } else {
            array_push($_SESSION['cart'],["product" => $id,"amount"=> $amount] );
        }
    }

    public function editCart($product, $amount)
    {
        $_SESSION['cart'][$product->id]['amount'] = $amount;
    }

    public function removeFromCart($id)
    {
        unset($_SESSION['cart'][$id]);
    }

    public function clear()
    {
        unset($_SESSION['cart']);
        unset($_SESSION['kortingscode']);
        unset($_SESSION['errors']['kortingscode']);
    }

    private function calculateBtwTarief($btwtarief){
        return $btwtarief/100;
    }

  public function setPrice($price){
      $this->price += $price;
  }
  public function setBTW($price, $btwtarief){
      $this->btw += ($price* $this->calculateBtwTarief($btwtarief));
  }
  public function getPrice(){
      return round($this->price,2,1);
  }
  public function getBtw(){
      return round($this->btw,2,1);
  }

  public function getPriceIncBtw(){

        return round($this->price + $this->btw,2,1);
    }
}