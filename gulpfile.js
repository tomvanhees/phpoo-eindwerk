var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var uglycss = require('gulp-uglifycss');
var rename = require('gulp-rename');
var html_beautify = require('gulp-html-beautify');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var image = require('gulp-image');
var connect = require('gulp-connect');



gulp.task('css',function () {
    gulp.src([
            'dev/css/style.css'
        ]

    )
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(rename('styles.css'))
        .pipe(gulp.dest('dist/css'))
        .pipe(uglycss())
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest('dist/css'))
        .pipe(connect.reload())
});


// gulp.task('html',function () {
//     var options = {
//         indentSize: 2
//     };
//     gulp.src('src/html/**/*.html')
//         .pipe(html_beautify(options))
//         .pipe(gulp.dest("dist"))
//         .pipe(connect.reload())
//})

// gulp.task('javascript', function () {
//     var vendors =[
//         'src/vendor/**/*.js'
//     ]
//     gulp.src(vendors)
//         .pipe(concat('vendor.js'))
//         .pipe(gulp.dest('dist/js'))
//         .pipe(connect.reload())
//     gulp.src('src/js/**/*.js')
//         .pipe(uglify())
//         .pipe(concat('script.js'))
//         .pipe(gulp.dest('dist/js'))
//         .pipe(connect.reload())
// })



gulp.task('image', function () {
    gulp.src('dev/img/*')
        .pipe(image())
        .pipe(gulp.dest('dist/img'))
})

gulp.task('font',function () {
    gulp.src('dev/fonts/*')
        .pipe(gulp.dest('dist/fonts'))
})

gulp.task('connect',function () {
    connect.server({
        root:'phpoo-eindwerk',
        livereload:true
    })
})


gulp.task('watch',['css'], function () {
    gulp.watch('src/scss/**/*.scss',['css'])
    //gulp.watch('src/html/**/*.html',['html'])
    //gulp.watch('src/js/**/*.js',['javascript'])
    //gulp.watch('src/vendor/**/*.js',['javascript'])
    //gulp.watch('src/img/*',['image'])
    //gulp.watch('src/fonts/*',['font'])
})

gulp.task('init', ['watch']);