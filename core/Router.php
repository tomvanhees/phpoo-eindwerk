<?php

class Router
{
    public $routes = [
        "GET"  => [],
        "POST" => []
    ];

//    public function define($routes){
//        $this->routes = $routes;
//    }
    
    
    public function direct($uri, $requesttype)
    {
        if (array_key_exists($uri, $this->routes[$requesttype])) {
            return 'controllers/' . $this->routes[$requesttype][$uri] . '.php';
        } else {
            redirect("location: /");
        }
    }
    
    public function get($uri, $controller)
    {
        $this->routes['GET'][$uri] = $controller;
    }
    
    public function post($uri, $controller)
    {
        $this->routes['POST'][$uri] = $controller;
    }
    
    
}