<?php

    class Connection
    {
        // STATIC function: je moet geen object aanmaken om gebruik te maken van deze functie
        public static function make()
        {
            //try and catch -> probeer, indien niet gelukt->actie

            try {
                $pdo =  new PDO('mysql:host=localhost;dbname=bakker', 'root', '');
                $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                return $pdo;
            }
            catch (PDOException $e){
                die($e->getMessage());
            }
        }
    }