<?php

class Querybuilder
{

  public $pdo;

  public function __construct($pdo)
  {
    $this->pdo = $pdo;
  }

  public function selectAll($table)
  {
    $classname = substr(ucfirst($table), 0, -1);

    $statement = $this->pdo->prepare("select * from {$table}");
    $statement->execute();
    return $statement->fetchAll(PDO::FETCH_CLASS, $classname);
  }

  public function selectAllById($table, $id)
  {
    $classname = substr(ucfirst($table), 0, -1);

    $statement = $this->pdo->prepare("select * from {$table} where id = {$id}");
    $statement->execute();
    return $statement->fetchAll(PDO::FETCH_CLASS, $classname)[0];
  }

  public function selectAllInnerJoin($table, $parameters)
  {
    $set = "";
    $classname = substr(ucfirst($table), 0, -1);

    foreach ($parameters as $parameter) {
      $set .= "INNER JOIN $parameter[0] ON $table.$parameter[1] = $parameter[0].$parameter[2] ";
    }
    $sql = "SELECT * FROM $table $set";
    try {
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_CLASS, $classname);
    } catch (PDOException $e) {
      die($e->getMessage());
    }

  }

  public function selectAllLike($table, $column, $pattern)
  {
    $classname = substr(ucfirst($table), 0, -1);
    $sql = sprintf('Select * From %s where %s like "%s"',
         $table,$column,$pattern);
    try {
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS, $classname)[0];
      } catch (PDOException $e) {
      die($e->getMessage());
    }
  }

  public function selectAllInnerJoinById($table, $parameters, $id)
  {
    $set = "";
    $classname = substr(ucfirst($table), 0, -1);

    foreach ($parameters as $parameter) {
      $set .= "INNER JOIN $parameter[0] ON $table.$parameter[1] = $parameter[0].$parameter[2] ";
    }
    $sql = "SELECT * FROM $table $set WHERE $table" . "_id = :id";
    try {
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute([
          "id" => $id,
      ]);
      return $stmt->fetchAll(PDO::FETCH_CLASS, $classname)[0];
    } catch (PDOException $e) {
      die($e->getMessage());
    }
  }


  public function removeById($table, $id)
  {
    $statement = $this->pdo->prepare("delete from {$table} where products_id = {$id}");
    $statement->execute();
  }

  public function insert($table, $parameters)
  {
    $sql = sprintf('INSERT INTO %s (%s) VALUES (%s)',
        $table,
        implode(', ', array_keys($parameters)),
        ':' . implode(', :', array_keys($parameters)));
    try {
      $stmt = $this->pdo->prepare($sql);
      $stmt->execute($parameters);
    } catch (PDOException $e) {
      die($e->getMessage());
    }
  }

  public function update($table, $parameters)
  {
    $id = array_pop($parameters);
    $set = "";

    foreach ($parameters as $key => $value) {
      $set .= $key . "= :" . $key . ",";
    }

    $set = substr($set, 0, -1);
    echo $sql = sprintf("UPDATE %s SET %s WHERE $table" . "_id = %s",
        $table,
        $set,
        $id);
    try {
      $stmt = $this->pdo->prepare($sql);
//      d($parameters);
      echo $stmt->execute($parameters);

    } catch (PDOException $e) {
    }

  }


  public function getUser($table, $username)
  {
    $classname = substr(ucfirst($table), 0, -1);
    $statement = $this->pdo->prepare("select * from {$table} where username='{$username}'");
    $statement->execute();
    return $statement->fetchAll(PDO::FETCH_CLASS, $classname)[0];
  }
}