<?php

require_once('core/database/Connection.php');
require_once('inc/functions.inc.php');
require_once('classes/Product.php');
require_once('classes/User.php');
require_once('core/database/Querybuilder.php');
require_once('core/Request.php');
require_once('classes/Cart.php');
require_once('classes/Kortingscode.php');
require_once('classes/Score.php');
require_once('classes/Categorie.php');

$zaak = "bakkerij";
$pdo = Connection::make();
$query = new Querybuilder($pdo);
session_start();
if(!isset($_SESSION['korting'])){
  $_SESSION['korting'] = FALSE;
}


$cart = new Cart();

