<?php

if (isset($_SESSION['user'])){
    $router->get('product/edit','admin/product_edit');
    $router->get('product/add' , 'admin/product_add');
    $router->get('product/delete' , 'admin/product_delete');

    $router->get('admin','admin/admin');

    $router->post('product/edit','admin/product_update');
    $router->post('product/add','admin/product_insert');
}

$router->get('winkelwagen', 'cart');
$router->get('','index');
$router->get('index','index');
$router->get('about','about');
$router->get('product','product');
$router->get('beoordeel','beoordeel');


$router->get('cart/delete' , 'cart_remove');
$router->get('login', 'login');
$router->get('logout','logout');
$router->get('winkelwagen/bestel','cart_order');



$router->post('login-check', 'login-post');
$router->post('cart/add','cart_add');
$router->post('cart/update','cart_update');
$router->post('cart/kortingscode','kortingscode_add');