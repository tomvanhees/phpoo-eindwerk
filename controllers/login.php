<?php



//ALS ik ingelogd ben --> doorsturen naar index pagina
if (isset($_SESSION['user'])) {
    header('location: index.php');
    exit;
}

$title = "Login | $zaak";
require_once ('views/login.view.php');