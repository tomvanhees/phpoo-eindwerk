<?php

if (empty($_POST['naam'])) {
    $errors['naam'] = "Naam is verplicht!";
}
if (empty($_POST['omschrijving'])) {
    $errors['omschrijving'] = "Omschrijving is verplicht!";
}
if (empty($_POST['prijs'])) {
    $errors['prijs'] = "Prijs is verplicht!";
}
if (empty($_POST['btw_id'])) {
    $errors['btw_id'] = "Btwtarief is verplicht!";
}
if (empty($_POST['categorie_id'])) {
    $errors['categorie_id'] = "Categorie_id is verplicht!";
}
if (empty($_POST['images'])) {
    $errors['images'] = "Images is verplicht!";
}