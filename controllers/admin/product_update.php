<?php

$title = "update | bakkerij";



$errors = [];
if ($_POST) {
require('validation.php');
if (empty($errors)) {

    $query->update('products', [
        'naam' => $_POST['naam'],
        'omschrijving' => $_POST['omschrijving'],
        'prijs' => $_POST['prijs'],
        'btw_id' => $_POST['btw_id'],
        'scores_id' => 1,
        'categorie_id' => $_POST['categorie_id'],
        'images' => $_POST['images'],
        'id' => $_GET['id']

    ]);
    header("location: /admin");
    exit;

}
 }