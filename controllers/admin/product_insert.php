<?php
$title = "add | bakkerij";

$errors = [];
if ($_POST) {
    require('validation.php');
    if (empty($errors)) {
        $query->insert('scores', [
            'totale_score'=> 3,
            'aantal_kliks'=>1,
            'gemiddelde'=>3
        ]);
       $scores=count($query->selectAll('scores'));
        $query->insert('products', [
            'naam' => $_POST['naam'],
            'omschrijving' => $_POST['omschrijving'],
            'prijs' => $_POST['prijs'],
            'btw_id' => $_POST['btw_id'],
            'scores_id'=> $scores,
            'categorie_id' => $_POST['categorie_id'],
            'images' => $_POST['images'],
        ]);
        header('location: /admin');
        exit;

    }
}
require ('./views/admin/product_add.view.php');
