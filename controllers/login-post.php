<?php




//ALS ik ingelogd ben --> doorsturen naar index pagina
if (isset($_SESSION['user'])) {
header('location: index');
exit;
}

$errors = [];


$username = $_POST['username'];

if (empty($_POST['username'])) {
$errors['username'] = "Gebruikersnaam is verplicht!";
}

if (empty($_POST['password'])) {
$errors['password'] = "Wachtwoord is verplicht!";
}

if (empty($errors)) {

    $user = $query->getUser('users', $username);


    //Controleren of er een user gevonden is
    if ($user) {
        //Als er een user gevonden is -> wachtwoord controleren
        if ($user->password == $_POST['password']) {
            // als ww correct is -> inloggen
            $_SESSION['user'] = $user;
            //doorsturen naar admin pagina
            header('location: admin');
            exit;
        } else {
            $errors['username'] = "De combinatie username en wachtwoord is niet correct";
        }
    } else {
        $errors['username'] = "Er kunnen geen gegevens gevonden worden voor deze username";
    }

}

$title = "Login | $zaak";
require_once ('views/login.view.php');