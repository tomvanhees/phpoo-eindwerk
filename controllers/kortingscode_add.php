<?php

/*Toevoegen en controleren op kortingscodes
---------------------------------------------
 *
 * Controleer of sessie al bestaat
 *  => TRUE  : geef foutmelding weer
 *  => FALSE :
 *
 * Controleer of minimum prijs correct is
 *  =>TRUE : haal code op die gelijk is aan de input
 * =>FALSE : geef foutmedling
 *
 *  Controleer of de code bestaat
 *  =>TRUE  : geef foutmedling
 *  =>FALSE :
 *
 *  Controleer of de code actief is
 *  =>TRUE  : Zet Sessie, deactiveer code , reset errors
 *  =>False : Geef foutmedling
 *
 * */




if ($_SESSION['korting'] == FALSE) {

  if ($_POST['prijs'] > 10) {
    $kortingscode = $query->selectAllLike("kortingscodes", "code", $_POST['code']);

    if ($kortingscode === NULL) {
      $error= "Uw code is niet geldig";
    } else {

      if ($kortingscode->actief == 0) {
        $_SESSION['korting'] = TRUE;
        $query->update('kortingscodes', ['actief' => 1, 'kortingscode' => $kortingscode->kortingscodes_id]);
        $error = "";
      } else {
        $error = "Deze code is niet meer beschikbaar";
      }
    }
  } else {
    $error = "Uw code is pas beschikbaar voor een bedrag hoger dan &euro;10";
  }


} else {
  $error = "U heeft al een kortingscode ingegeven.";
}

$_SESSION['errors']['kortingscode'] = $error;

header('location: /winkelwagen');