<?php


/* Verwijder de Sessie als ze niet gebruikt wordt*/
if (empty($_SESSION['cart'])) {
  $cart->clear();
}



if (isset($_SESSION['cart'])) {
  $products = [];


  /* Maakt een array met product objecten
    -----------------------------------------
   *
   * @parameter: $item['product'] = Bestaat uit de id van het product
   * @parameter: $item['amount'] = Bevat hoeveel eenheden we nodig hebben van het product
   * $key = Wordt gebruikt om de plek aan te geven waar het product zich in de cart bevind. Dit is nodig voor de update.
   * @function: $setPrice & $setBtw = updaten de totale prijs en btw van de winkelwagen.
   * */



  foreach ($_SESSION['cart'] as $key => $item) {
    $product = $query->selectAllInnerJoinById("products", [["scores", "scores_id", "scores_id"],
        ["btw", "btw_id", "id"]], $item['product']);
    $product->amount = $item['amount'];
    $product->cartLocation = $key;
    $cart->setPrice($product->prijs * $product->amount);
    $cart->setBTW($product->prijs * $product->amount, $product->btwtarief);
    array_push($products,$product);


  }
}

$besteld = false;
$title = "winkelwagen | $zaak";
require_once('views/cart.view.php');