<?php


/* Updaten van de hoeveelheid items in de cart
------------------------------------------------
 *
 * overloop de array
 * => controleer of de waarde niet onder 1 is. Dit is een extra controle want deze wordt ook al gecontroleerd door de html-tag
 *      TRUE  => voeg error toe
 *      FALSE => update waarde in de cart
 *            => verwijder eventuele errors
 * */




foreach ($_SESSION['cart'] as $key => $value) {
    $value =  $_POST[$key.'amount'];
    if( $value < 1){
        $_SESSION['errors']['cart'][$key] = "Uw waarde mag niet onder 1 zijn";
    }else{
            $_SESSION['cart'][$key]["amount"] = $value;
            if(isset($_SESSION['errors']['cart'][$key])){
                unset($_SESSION['errors']['cart'][$key]);
            }
        }


}

header("location: {$_SERVER['HTTP_REFERER']}");