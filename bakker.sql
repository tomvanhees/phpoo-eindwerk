-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 23, 2018 at 10:50 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bakker`
--
CREATE DATABASE IF NOT EXISTS `bakker` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bakker`;

-- --------------------------------------------------------

--
-- Table structure for table `btw`
--

DROP TABLE IF EXISTS `btw`;
CREATE TABLE IF NOT EXISTS `btw` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `btwtarief` decimal(11,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `btw`
--

INSERT INTO `btw` (`id`, `btwtarief`) VALUES
(1, '6.00'),
(2, '21.00');

-- --------------------------------------------------------

--
-- Table structure for table `categorien`
--

DROP TABLE IF EXISTS `categorien`;
CREATE TABLE IF NOT EXISTS `categorien` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categorie_naam` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categorien`
--

INSERT INTO `categorien` (`id`, `categorie_naam`) VALUES
(1, 'Brood'),
(2, 'Koffiekoeken'),
(3, 'Pistolets'),
(4, 'Gebakjes'),
(5, 'Taarten'),
(6, 'Hartige snacks');

-- --------------------------------------------------------

--
-- Table structure for table `kortingscodes`
--

DROP TABLE IF EXISTS `kortingscodes`;
CREATE TABLE IF NOT EXISTS `kortingscodes` (
  `kortingscodes_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(12) NOT NULL,
  `actief` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`kortingscodes_id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kortingscodes`
--

INSERT INTO `kortingscodes` (`kortingscodes_id`, `code`, `actief`) VALUES
(1, 'TOSTICA00000', 1),
(2, 'TOSTICA00001', 1),
(3, 'TOSTICA00002', 0),
(4, 'TOSTICA00003', 0);

-- --------------------------------------------------------

--
-- Table structure for table `maat`
--

DROP TABLE IF EXISTS `maat`;
CREATE TABLE IF NOT EXISTS `maat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `soort` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `maat`
--

INSERT INTO `maat` (`id`, `soort`) VALUES
(1, 'Groot'),
(2, 'Klein');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `products_id` int(11) NOT NULL AUTO_INCREMENT,
  `naam` varchar(255) NOT NULL,
  `omschrijving` text NOT NULL,
  `prijs` decimal(10,2) NOT NULL,
  `btw_id` int(11) NOT NULL,
  `scores_id` int(11) NOT NULL,
  `categorie_id` int(10) NOT NULL,
  `images` varchar(255) NOT NULL,
  PRIMARY KEY (`products_id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`products_id`, `naam`, `omschrijving`, `prijs`, `btw_id`, `scores_id`, `categorie_id`, `images`) VALUES
(3, 'Lang wit brood (groot)', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor sdf', '2.20', 1, 1, 1, './dist/img/langwit.jpg'),
(4, 'Lang wit brood(klein)', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor ', '1.60', 1, 4, 1, './dist/img/langwit.jpg'),
(1, 'Rond wit brood (groot)', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor ', '2.20', 1, 1, 1, './dist/img/rondwit.jpg'),
(2, 'Rond wit brood (klein)', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor ', '2.20', 2, 1, 1, './dist/img/rondwit.jpg'),
(5, 'eclair', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '1.40', 2, 1, 2, './dist/img/eclair.jpg'),
(6, 'Aardbeientaart', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '9.00', 2, 1, 5, './dist/img/aardbeientaart.jpg'),
(7, 'Piccolo', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '0.40', 2, 1, 3, './dist/img/piccolo.jpg'),
(8, 'Abricoostaart 4pers', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '4.25', 2, 5, 5, './dist/img/abricoostaart.jpg'),
(9, 'Abricoostaart 6pers', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '5.25', 2, 6, 5, './dist/img/abricoostaart.jpg'),
(10, 'Abricoostaart 8 pers', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '6.25', 2, 7, 5, './dist/img/abricoostaart.jpg'),
(12, 'Javanais', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '3.00', 2, 9, 4, './dist/img/javanais.jpg'),
(13, 'Worstenbroodje', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '1.90', 2, 10, 6, './dist/img/worstenbroodje.jpg'),
(14, 'volkoren brood', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '2.20', 2, 14, 1, './dist/img/volkoren.jpg'),
(15, 'Spelt brood', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '2.20', 1, 15, 1, './dist/img/speltbrood.jpg'),
(16, 'Suikerbrood', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '3.10', 1, 16, 1, './dist/img/suikerbrood.jpg'),
(17, 'Abricozenflap', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '1.20', 1, 17, 2, './dist/img/abricozenflap.jpg'),
(18, 'Appelflap', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '1.40', 1, 18, 2, './dist/img/appelflap.jpg'),
(19, 'Chocoladesoes', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '2.20', 1, 19, 4, './dist/img/chocoladesoes.jpg'),
(20, 'Smurfentaart 4 pers', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '5.50', 1, 20, 5, './dist/img/smurfentaart.jpg'),
(21, 'Spinnentaart 8 pers', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '8.00', 1, 21, 5, './dist/img/spinnentaart.jpg'),
(22, 'Tijger pistolet', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '0.45', 1, 22, 3, './dist/img/tijgerpistolet.jpg'),
(23, 'Waldkorn pistolet', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '0.45', 2, 23, 3, './dist/img/waldkornpistolet.jpg'),
(24, 'Stokbrood grijs', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '0.85', 2, 24, 3, './dist/img/stokbroodgrijs.jpg'),
(25, 'Mini donutspiesje', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '1.80', 1, 25, 2, './dist/img/minidonutspiesj.jpg'),
(26, 'Lang grijs (groot)', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '2.20', 1, 26, 1, './dist/img/langgrijs.jpg'),
(27, 'Lang grijs (klein)', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '1.60', 1, 27, 1, './dist/img/langgrijs.jpg'),
(28, 'Rond grijs (groot)', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '1.60', 1, 28, 1, './dist/img/rondgrijs.jpg'),
(29, 'bananentaart 8pers', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '11.00', 1, 29, 5, './dist/img/bananentaart.jpg'),
(30, 'Berlijnse bol', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '1.40', 1, 30, 2, './dist/img/berlijnsebol.jpg'),
(31, 'Framboos bol', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '3.00', 1, 31, 4, './dist/img/framboosbol.jpg'),
(32, 'Biscuit crÃ¨me au beurre vanille', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '2.20', 1, 32, 4, './dist/img/buscuitcremeaubeurre.jpg'),
(33, 'Merveilleux', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '2.20', 1, 33, 4, './dist/img/merveilleux.jpg'),
(34, 'Samba', 'loremipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', '3.00', 1, 34, 4, './dist/img/samba.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `scores`
--

DROP TABLE IF EXISTS `scores`;
CREATE TABLE IF NOT EXISTS `scores` (
  `scores_id` int(11) NOT NULL AUTO_INCREMENT,
  `totale_score` int(11) NOT NULL,
  `aantal_kliks` int(11) NOT NULL,
  `gemiddelde` int(11) NOT NULL,
  PRIMARY KEY (`scores_id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scores`
--

INSERT INTO `scores` (`scores_id`, `totale_score`, `aantal_kliks`, `gemiddelde`) VALUES
(1, 11, 3, 4),
(2, 297, 72, 4),
(3, 134, 52, 3),
(4, 72, 33, 2),
(5, 3, 1, 3),
(6, 3, 1, 3),
(7, 3, 1, 3),
(8, 3, 1, 3),
(9, 3, 1, 3),
(10, 8, 2, 4),
(11, 3, 1, 3),
(12, 3, 1, 3),
(13, 3, 1, 3),
(14, 6, 2, 3),
(15, 3, 1, 3),
(16, 3, 1, 3),
(17, 3, 1, 3),
(25, 3, 1, 3),
(24, 3, 1, 3),
(23, 3, 1, 3),
(22, 3, 1, 3),
(26, 3, 1, 3),
(27, 3, 1, 3),
(28, 3, 1, 3),
(29, 3, 1, 3),
(30, 3, 1, 3),
(31, 3, 1, 3),
(32, 3, 1, 3),
(33, 3, 1, 3),
(34, 3, 1, 3),
(35, 3, 1, 3),
(36, 3, 1, 3),
(37, 3, 1, 3),
(38, 3, 1, 3),
(39, 3, 1, 3),
(40, 3, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`) VALUES
(1, 'admin', 'admin');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
