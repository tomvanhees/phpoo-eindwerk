<p>
    <label class="block text-grey-darker text-sm font-bold mb-2 pt-8" for="naam">Naam</label>
    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker" type="text" id="naam" name="naam" value="<?= $product->naam ?? '' ?>">
     <?php if (isset($errors['naam'])): ?>
      <small class="text-red-dark"><?= $errors['naam'] ?></small>
    <?php endif ?>
</p>

<p>
    <label class="block text-grey-darker text-sm font-bold mb-2 pt-8" for="omschrijving">Omschrijving</label>
    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker" type="text" id="omschrijving" name="omschrijving" value="<?= $product->omschrijving ?? '' ?>">
     <?php if (isset($errors['omschrijving'])): ?>
      <small class="text-red-dark"><?= $errors['omschrijving'] ?></small>
    <?php endif ?>
</p>

<p>
    <label class="block text-grey-darker text-sm font-bold mb-2 pt-8" for="prijs">Prijs</label>
    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker" type="text" id="prijs" name="prijs" value="<?= $product->prijs ?? '' ?>">
  <?php if (isset($errors['prijs'])): ?>
      <small class="text-red-dark"><?= $errors['prijs'] ?></small>php
    <?php endif ?>
</p> 

<p>
    <label class="block text-grey-darker text-sm font-bold mb-2 pt-8" for="btw_id">btw tarief</label>
    <select class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker"  name="btw_id" id="btw_id" >
        <option>Klik hier om een keuze te maken</option>
        <option value="1" <?php if(ISSET($product)):?> <?= $product->btw_id==1? 'selected': '' ?> <?php endif ?> >6%</option>
        <option value="2" <?php if(ISSET($product)):?> <?= $product->btw_id==2? 'selected': '' ?> <?php endif ?> >21%</option>
    </select>
   <?php if (isset($errors['btw_id'])): ?>
      <small class="text-red-dark"><?= $errors['btw_id'] ?></small>
    <?php endif ?>
</p>

<p>
    <label class="block text-grey-darker text-sm font-bold mb-2 pt-8" for="categorie_id">Categorie</label>
    <select class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker"  name="categorie_id" id="categorie_id" >
        <option>Klik hier om een keuze te maken</option>
        <?php foreach($categorie as $categorie_item):?>
        <option value="<?= $categorie_item->id?>" <?php if(ISSET($product)):?> <?= $categorie_item->id==$product->categorie_id? 'selected': '' ?> <?php endif ?> ><?= $categorie_item->categorie_naam?></option>
        <?php endforeach;?>
    </select>
     <?php if (isset($errors['categorie_id'])): ?>
      <small class="text-red-dark"><?= $errors['categorie_id'] ?></small>
    <?php endif ?>
</p>
<p>
    <label class="block text-grey-darker text-sm font-bold mb-2 pt-8" for="images">Foto</label>
    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker" type="text" id="images" name="images" value="<?= $product->images ?? './dist/img/' ?>">
     <?php if (isset($errors['images'])): ?>
      <small class="text-red-dark"><?= $errors['images'] ?></small>
    <?php endif ?>
</p>
