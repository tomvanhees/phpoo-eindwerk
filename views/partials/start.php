<!DOCTYPE html>
<html lang="nl">

<head>
    <meta charset="UTF-8">
    <title><?= $title ?></title>
    <link rel="stylesheet" href="/dist/css/tailwind.css">
    <link rel="stylesheet" href="/dist/css/styles.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <script src="/dist/js/scrollToTop.js"></script>
</head>
<?php if(isset($_SESSION['position'])): ?>
<body onload="window.scrollTo(0,<?= $_SESSION['position'] ?? 0 ?>) ">
<?php elseif(isset($_SESSION['id'])): ?>
<body onload="window.scrollTo(0,document.getElementById(<?= $_SESSION['id']?>).offsetTop-(window.innerHeight/2)) ">
<?php else: ?>
<body>
<?php endif; ?>
<?php unset($_SESSION['position']); unset($_SESSION['id']) ?>
<header class="h-64 bg-cover">
    <div class="container mx-auto">
        <nav class="flex items-center justify-between flex-wrap bg-transparent p-8 pb-0 w-full">
            <div class="w-full block flex-grow lg:flex lg:items-center lg:w-auto container mx-auto pb-6">
                <div class="text-sm lg:flex-grow flex justify-between">

                    <a href="/about"
                       class="block mt-4 lg:inline-block lg:mt-0 text-white hover:text-white mr-4 no-underline text-xl">
                        Over ons
                    </a>
                    <a href="/"
                       class="block mt-4 lg:inline-block lg:mt-0 text-white hover:text-white mr-4 no-underline">
                        <img src="/dist/img/logo.png" alt="Logo tostica" class="logo">
                    </a>
                    <a href="/winkelwagen"
                       class="block mt-4 lg:inline-block lg:mt-0 text-white hover:text-white mr-4 no-underline text-xl ">
                      Bestel online <i class="fas fa-shopping-cart"></i>
                      <span>(<?= isset($_SESSION['cart']) ? count($_SESSION['cart']) : "0" ?>)</span>
                    </a>
                </div>
            </div>
        </nav>

        <div class="flex items-center flex-wrap bg-transparent p-6 w-full">
            <div class="w-full block flex-grow lg:flex lg:items-center lg:w-auto container mx-auto ">
                <div class="justify-center lg:flex-grow flex border-t-2 border-black border-b-2">

                    <a href="/#Brood"
                       class="block mt-4 lg:inline-block lg:mt-0 text-white hover:text-white mr-4 no-underline text-lg p-2">
                        Broodjes
                    </a>
                    <a href="/#Koffiekoeken"
                       class="block mt-4 lg:inline-block lg:mt-0 text-white hover:text-white mr-4 no-underline text-lg p-2">
                        Koffiekoeken
                    </a>
                    <a href="/#Pistolets"
                       class="block mt-4 lg:inline-block lg:mt-0 text-white hover:text-white mr-4 no-underline text-lg p-2">
                        Pistolets
                    </a>
                    <a href="/#Gebakjes"
                       class="block mt-4 lg:inline-block lg:mt-0 text-white hover:text-white mr-4 no-underline text-lg p-2">
                        Gebakjes
                    </a>
                    <a href="/#Taarten"
                       class="block mt-4 lg:inline-block lg:mt-0 text-white hover:text-white mr-4 no-underline text-lg p-2">
                        Taarten
                    </a>
                    <a href="/#Hartige snacks"
                       class="block mt-4 lg:inline-block lg:mt-0 text-white hover:text-white mr-4 no-underline text-lg p-2">
                        Hartige snacks
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>