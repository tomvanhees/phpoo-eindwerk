<div class="w-1/5 rounded border border-grey m-2 mb-8 mr-8">
    <a href="/product?id=<?= $product->products_id ?>"><img class="w-full" src="<?= $product->images ?>" alt="<?= $product->naam ?>"></a>
    <div class="px-4 py-4">
        <div class="font-bold text-lg mb-6"><a href="/product?id=<?= $product->products_id ?>" class="no-underline text-black hover:text-grey-darkest"><?= $product->naam ?></a>
        </div>

        <div id="<?= $product->products_id ?>"><?php
          for ($i = 1; $i <= $product->gemiddelde; $i++) {
            echo "<a class='pr-1' href='beoordeel?product=$product->products_id&beoordeling=" . $i . "' title='" . $i . " ster' onFocus='this.blur()'><img src='../dist/img/beoordeling.png' alt='beoordeling'></a>";
          }
          for ($i = $product->gemiddelde + 1; $i <= 5; $i++) {
            echo "<a class='pr-1' href='beoordeel?product=$product->products_id&beoordeling=" . $i . "' title='" . $i . " ster' onFocus='this.blur()'><img src='../dist/img/beoordeling-geen.png' alt='beoordeling'></a>";
          }
          ?> (<?= $product->aantal_kliks ?>)

            <p class="pt-4">€ <?= $product->getPrijsIncBtwPP() ?></p>
        </div>


    </div>
    <div class="px-4 py-4 flex justify-between">
        <form action="cart/add" method="post" class="w-full max-w-sm" onsubmit="this.position.value=window.pageYOffset">
          <input type="hidden" name="position">
            <div class="flex items-center border-b border-b-2 border-grey-light py-2">
                <input name="id" value="<?= $product->products_id ?>" hidden>
            <label for="aantal" class="inline-block w-1/4 py-2">Aantal</label>
            <input type="text" value="1" class="w-1/4 inline border p-1" name="amount">
            <input type="submit" class="flex bg-red text-white font-bold py-2 px-4 ml-8 rounded hover:bg-red-dark cursor-pointer" id="btn" value="Bestel">
            </div>
        </form>
    </div>
</div>

