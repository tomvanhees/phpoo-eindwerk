
<div class="container mx-auto mt-8 mb-6 text-center">
    &copy; <?= date("Y"); ?> Tostica -
<?php if(isset($_SESSION['user'])):?>

    <a href="/logout"  class="no-underline text-black hover:text-grey-darkest">Logout</a>

    - Logged in as <b><?= $_SESSION['user']->username ?></b> - Go to <a href="admin"  class="no-underline text-black hover:text-grey-darkest">admin page</a>

<?php else: ?> <!-- elseif(basename($_SERVER['PHP_SELF']) != 'login.php'): > als je de login niet op de loginpagina onder de button wilt -->


    <a href="/login" class="no-underline text-black hover:text-grey-darkest">Login</a>

<?php endif; ?>
</div>
</body>

</html>