<?php require('./views/partials/start.php'); ?>
<div class="container mx-auto">
    <button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>

        <?php foreach ($categorien as $categorie) : ?>

            <h1 id="<?= $categorie->categorie_naam?>" class="mb-8 mt-8 pt-4"><?= $categorie->categorie_naam?></h1>

            <div class="flex overflow-hidden flex-wrap mx-auto">
                <?php foreach ($products as $product) : ?>
                    <?php if($product->categorie_id == $categorie->id) : ?>
                        <?php require './views/partials/card.partial.php' ?>
                    <?php endif ?>
                <?php endforeach; ?>


            </div>

        <?php endforeach ?>


    <?php require('./views/partials/end.php') ?>

