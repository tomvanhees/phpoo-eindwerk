<?php require('./views/partials/start.php'); ?>

<div class="container mx-auto">
    <div class="mt-6 border-dashed border-b-2 pt-2">
    <h1 class="text-center mt-6 pb-8 uppercase">Over ons</h1>
       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid deleniti odio officia possimus totam? Beatae dolore ducimus est facere in, ipsum perspiciatis quasi vitae. At laboriosam officiis quaerat? Aliquam aspernatur assumenda, at aut blanditiis consectetur dicta dolorem dolorum error eum exercitationem harum laborum minus, molestiae molestias nisi nobis numquam odit officia officiis pariatur porro provident quaerat quam qui quia quisquam, rem repellendus repudiandae rerum sequi sint tempore temporibus tenetur totam veritatis voluptas? Atque cum, dicta eum iusto nulla quidem unde.</p>
        <p class="pb-6">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores non possimus quisquam tempore. A aliquam amet assumenda, atque deleniti ex excepturi ipsam iusto laudantium magni odit qui quia quo saepe sed sunt ullam vel! Asperiores deserunt, eaque explicabo hic ipsum itaque iure nesciunt pariatur quisquam suscipit tempora ut vero. Facilis?</ppb-8>
</div>
    <div class="flex mt-8">
        <div class="flex-1 text-center px-4 py-2 m-2 border-r-2">
            <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
            <h2 class=" mt-3 mb-3">Steffi</h2>
            <p class="text-left">Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
        </div><!-- /.col-lg-4 -->
        <div class="flex-1 text-center px-4 py-2 m-2 border-r-2">
            <img class="rounded-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
            <h2 class=" mt-3 mb-3">Tom</h2>
            <p class="text-left">Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.</p>
        </div><!-- /.col-lg-4 -->
        <div class="flex-1 text-center px-4 py-2 m-2">
            <img class="rounded-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
            <h2 class=" mt-3 mb-3">Jessica</h2>
            <p class="text-left">Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
        </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->

    <?php require('./views/partials/end.php'); ?>
