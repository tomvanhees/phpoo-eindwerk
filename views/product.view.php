<?php

require_once ('./views/partials/start.php');
?>

<div class="container mx-auto">
    <p class="mb-8 mt-8"><a href="/index" class="bg-grey-light text-black font-bold py-2 px-4 mt-8 ml-8 rounded no-underline hover:text-grey-darkest">Terug</a></p>
    <div class="max-w-md w-full ml-auto mr-auto lg:flex">
        <div class="border-l border-b border-t border-grey-light h-48 lg:h-auto lg:w-48 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden">
            <img src="<?= $product->images?>" alt="<?= $product->naam?>">
        </div>
        <div class="border-r border-b border-l border-grey-light lg:border-l-0 lg:border-t lg:border-grey-light bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal">
            <div class="mb-8">
                <div class="text-black font-bold text-xl mb-2"><?= $product->naam?></div>
                <p class="text-grey-darker text-base mb-6"><?= $product->omschrijving?></p>
                <p class="mb-4"><?php
                    for ($i = 1; $i <= $product->gemiddelde; $i++) {
                        echo "<a class='pr-1' href='beoordeel?product=$product->products_id&beoordeling=" . $i . "' title='" . $i . " ster' onFocus='this.blur()'><img src='../dist/img/beoordeling.png' alt='beoordeling'></a>";
                    }
                    for ($i = $product->gemiddelde + 1; $i <= 5; $i++) {
                        echo "<a class='pr-1' href='beoordeel?product=$product->products_id&beoordeling=" . $i . "' title='" . $i . " ster' onFocus='this.blur()'><img src='../dist/img/beoordeling-geen.png' alt='beoordeling'></a>";
                    }
                    ?> (<?= $product->aantal_kliks ?>)
                <p class="mb-2"><?= $product->omschrijving?></p>
                <p class="font-bold">€ <?= $product->getPrijsIncBtwPP() ?></p>

            </div>
            <div class="px-6 py-4 flex justify-between">
              <form action="cart/add" method="post" class="w-full max-w-sm">
                <div class="flex items-center border-b border-b-2 border-grey-light py-2">
                  <input name="id" value="<?= $product->products_id ?>" hidden>
                  <label for="aantal" class="inline-block w-1/4 py-2">Aantal</label>
                  <input type="text" value="1" class="w-1/4 inline border" name="amount">
                  <input type="submit" class="flex bg-red text-white font-bold py-2 px-4 ml-8 rounded hover:bg-red-dark" id="btn" value="Bestel">
                </div>
              </form>
            </div>
        </div>
    </div>
</div>


<?php

require_once ('./views/partials/end.php');
?>