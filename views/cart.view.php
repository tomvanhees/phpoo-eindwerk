<?php

require('views/partials/start.php');
?>

  <div class="flex mb-4">
    <div class="w-1/4 mt-8 bg-grey-lighter h-1/2 border-1 rounded ml-auto mr-auto">
      <?php if (!empty($_SESSION['cart'])): ?>
        <form action="cart/update" method="post" name="cart_update">
          <?php foreach ($products as $key => $product): ?>
            <div class="flex">
              <div class="h-48 lg:h-auto lg:w-48 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden mt-4 ml-4" style="background-image: url(<?= $product->images ?>)" title="Woman holding a mug">
              </div>
              <div class="flex w-full">
                <div class="w-2/5"></div>
                <div class="w-3/5 pt-4">
                  <div class="flex flex-col">
                    <div class="mt-1 text-xl">
                      <?= $product->naam ?>
                    </div>
                    <div class="mt-2">
                      <span class="text-sm">Aantal:</span>
                      <input type="number" min="1"
                             name="<?= $key ?>amount"
                             value="<?= $product->amount ?>"
                             class="w-12 ml-4">
                        <p class="pb-4 text-red text-sm"><?= isset($_SESSION['errors']['cart'][$key]) ? $_SESSION['errors']['cart'][$key] : "" ?></p>
                    </div>
                    <div class="mt-1">
                      <span class="text-sm">Totale prijs: </span><span class="">&euro;
                        <?= $product->getPrijsIncBtw() ?></span>
                    </div>
                    <div class="self-end mr-4 mt-6">
                      <a href="/cart/delete?id=<?= $product->cartLocation ?>" class="bg-grey-light font-medium text-black py-2 px-4 ml-8 rounded no-underline hover:text-grey-darkest">Verwijder</a></div>
                  </div>
                </div>
              </div>
            </div>
            <hr class="border border-grey mt-4">
          <?php endforeach; ?>
          <button type="submit"
                  class="bg-blue float-right hover:bg-blue-dark text-white font-bold py-2 px-4 mb-8 rounded-full mr-2">Update
          </button>
        </form>
        <div class="flex w-full">
          <div class="w-3/4 mt-8 ml-2">
            <form action="cart/kortingscode" method="post">
              <input type="text" value="<?= $cart->getPriceIncBtw() ?>" name="prijs" hidden>
              <p class="text-sm text-grey">Kortingscode</p>
              <input type="text"
                     class="shadow appearance-none border rounded py-2 px-3 text-grey-darker" name="code">
              <p class="mt-2">
                <button type="submit"
                        class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 mb-8 rounded-full">Verzenden
                </button>
              <p class="pb-4 text-red text-sm"><?= isset($_SESSION['errors']['kortingscode']) ? $_SESSION['errors']['kortingscode'] : "" ?></p>
              </p>
            </form>
          </div>
          <div class="flex flex-col mt-8 w-1/4">
            <p>Prijs:
              <span class="float-right mr-4"><?= $cart->getPrice() ?></span>
            </p>
            <p>Btw:
              <span class="float-right mr-4"><?= $cart->getBtw() ?></span>
            </p>
            <?php if($_SESSION['korting']): ?>
            <p>Korting:
              <span class="float-right mr-4">-10</span>
            </p>
            <?php endif; ?>
            <p class="mt-2">Totale prijs: <span class="float-right mr-4">&euro;
                <?= $_SESSION['korting'] ? $cart->getPriceIncBtw() - 10 : $cart->getPriceIncBtw() ?></span>
            </p>

          </div>

        </div>
        <hr class="border border-grey mt-4">
        <div class="flex w-full justify-end">
          <a href="/index" class="bg-blue text-center no-underline hover:bg-blue-dark mr-4 text-white font-bold py-2 px-4 mb-8 rounded-full ml-2">Verder winkelen</a>
          <a href="winkelwagen/bestel" class="bg-green text-center no-underline hover:bg-green-dark text-white font-bold py-2 px-4 mb-8 rounded-full mr-2">Bestel</a>
        </div>



      <?php else: ?>
        <p class="pt-8 pb-8 text-center"><?= $besteld ? "Dank U voor uw bestelling" : "U heeft nog geen items in je winkelwagen." ?></p>
      <?php endif; ?>


    </div>
  </div>

<?php
require('views/partials/end.php');
?>