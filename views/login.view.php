<?php require('./views/partials/start.php'); ?>
<div class="container mx-auto mt-8">
<div class="w-full max-w-xs mx-auto">
    <form method="post" action="/login-check" class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
        <div class="mb-4">
            <label class="block text-grey-darker text-sm font-bold mb-2" for="username">
                Gebruikersnaam
            </label>
            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker" id="username" name="username" type="text" placeholder="Gebruikersnaam" value="<?= $username ?? '' ?>">
            <small class="text-red"><?= $errors['username'] ?? ''?></small>
        </div>
        <div class="mb-6">
            <label class="block text-grey-darker text-sm font-bold mb-2" for="password">
                Wachtwoord
            </label>
            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker mb-3" id="password" name="password" type="password" placeholder="Wachtwoord">
<!--            <p class="text-red text-xs italic">Please choose a password.</p>-->
            <small class="text-red"><?= $errors['password'] ?? '' ?></small>
        </div>
        <div class="flex items-center justify-between">
            <button class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded" type="submit">
                Log in
            </button>
        </div>
    </form>
</div>
</div>

<?php require('./views/partials/end.php') ?>
