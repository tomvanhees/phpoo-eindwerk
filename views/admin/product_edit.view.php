<?php require('views/partials/start.php');

?>

    <div class="container mx-auto">

        <p class="float-right">
            <a href="/admin" class="btn btn-secondary">Terug</a>
        </p>

        <h1 class="mt-4 mb-4">Bewerk product: <?= $product->naam ?></a></h1>

        <form class="shadow-md rounded px-8 pt-6 pb-8 mb-4" method="post" action="/product/edit?id=<?= $product->products_id ?>">

            <?php require ('views/partials/form.view.php') ?>

            <p class="pt-8">
                <input type="submit" value="Save" class="btn btn-success">
            </p>

        </form>
    </div>

<?php require('views/partials/end.php'); ?>