<?php require('./views/partials/start.php');

//Als NIET ingelogd --> doorsturen naar login pagina


?>
    <div class="container mx-auto">
        <a href="/product/add"> <button class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded mt-8">
            Voeg nieuw product toe
        </button></a>
<table class="w-full mt-8">
    <tr class="text-left text-lg">
        <th class="border-b-2 border-solid">Naam</th>
        <th class="border-b-2 border-solid">Categorie</th>
        <th class="border-b-2 border-solid">Prijs</th>
        <th class="border-b-2 border-solid">BTW</th>
        <th class="border-b-2 border-solid">Gem. score</th>
        <th class="border-b-2 border-solid">Acties</th>
    </tr>

<?php foreach ($products as $product) : ?>
    <tr>
        <td><a href="/product?id=<?= $product->products_id ?>"  class="no-underline text-black hover:text-grey-darkest font-semibold"><?= $product->naam ?></a></td>
        <td><?= $product->categorie_naam ?></td>
        <td>&euro; <?= $product->prijs?></td>
        <td><?= $product->btwtarief?>%</td>
        <td><?php
            for ($i = 1; $i <= $product->gemiddelde; $i++) {
                echo "<a class='pr-1' href='beoordeel?product=$product->products_id&beoordeling=" . $i . "' title='" . $i . " ster' onFocus='this.blur()'><img src='../dist/img/beoordeling.png' alt='beoordeling'></a>";
            }
            for ($i = $product->gemiddelde + 1; $i <= 5; $i++) {
                echo "<a class='pr-1' href='beoordeel?product=$product->products_id&beoordeling=" . $i . "' title='" . $i . " ster' onFocus='this.blur()'><img src='../dist/img/beoordeling-geen.png' alt='beoordeling'></a>";
            }
            ?></td>
        <td><a href="/product/edit?id=<?= $product->products_id ?>"><button class="bg-orange hover:bg-orange-dark text-white font-bold py-2 px-4 rounded">
                Wijzig
            </button></a>
            <a href="/product/delete?id=<?= $product->products_id?>"><button class="bg-red hover:bg-red-dark text-white font-bold py-2 px-4 rounded">
                Verwijder
            </button></a>
        </td>
    </tr>
<?php endforeach; ?>
</table>
    </div>

<?php require('./views/partials/end.php') ?>